<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "provincias".
 *
 * @property string|null $autonomia
 * @property string $provincia
 * @property int|null $poblacion
 * @property int|null $superficie
 * @property string|null $festivo
 */
class Provincias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['provincia'], 'required'],
            [['poblacion', 'superficie'], 'integer'],
            
            [['autonomia', 'provincia'], 'string', 'max' => 255],
            [['provincia'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'autonomia' => 'Autonomia',
            'provincia' => 'Provincia',
            'poblacion' => 'Poblacion',
            'superficie' => 'Superficie',
            'festivo' => 'Festivo',
        ];
    }
    
//    public function afterFind() {
//        parent::afterFind();
//        $this->festivo=Yii::$app->formatter->asDate($this->festivo, 'php:d-m-Y');
//    }
//    
//    public function beforeSave($insert) {
//        parent::beforeSave($insert);
//        $this->festivo= \DateTime::createFromFormat("d/m/Y", $this->festivo)->format("Y/m/d");
//        return true;
//    }
}
